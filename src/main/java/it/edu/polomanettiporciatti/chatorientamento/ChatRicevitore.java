package it.edu.polomanettiporciatti.chatorientamento;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author 
 * @version 
 */

public class ChatRicevitore implements Runnable {

    private Socket inChannel;
    private BufferedReader buffer;
    
    public ChatRicevitore(Socket s) throws IOException, IllegalArgumentException{
        
        if(s.isConnected()){
            
            inChannel = s;
            try {
                
                buffer = new BufferedReader(new InputStreamReader(inChannel.getInputStream()));
                System.out.println("Ricevitore creato.");
            
            } catch (IOException ex){
                
                throw new IOException();
            
            }
        
        } else {
            
            throw new IllegalArgumentException();
        
        }
    
    }
    
    @Override
    public void run(){
        
        String message = "";
        
        do {
            
            try {
                
                message = buffer.readLine();
                if (!message.equals("/exit")){
                    System.out.print(inChannel.getInetAddress().toString() + " scrive: ");
                    System.out.println(message);
                }
            
            } catch (IOException ex) {
                
                return;
            
            }
        
        }while(!message.equals("/exit"));
        
    }
    
}